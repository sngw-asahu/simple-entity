using System;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class ArtilleryView : MonoBehaviour
{
    private const float Range = 10f;
    [SerializeField] public GameObject bullet;
    [CanBeNull] private GameObject _target;
    private float _timer;

    private void Start()
    {
        InvokeRepeating(nameof(Shot), 0f,0.8f);
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        
        if (_timer < 8f) return;
        Shot();
        _timer = 0f;
    }

    private void Shot()
    {
        if (_target == null)
        {
            _target = GetNearEntity();
            return;
        }
        var currentTransform = transform;

        if (Vector3.Distance(currentTransform.position, _target.transform.position) > Range || _target.GetComponent<EntityView>().needsDestroy)
        {
            _target = null;
            return;
        }
        transform.LookAt(_target.transform);
        var newBullet = Instantiate(bullet, currentTransform.position, Quaternion.identity);
        
        newBullet.GetComponent<Rigidbody>().AddForce(currentTransform.forward * 1000, ForceMode.Force);
        Destroy(newBullet, 3f);
    }

    [CanBeNull]
    private GameObject GetNearEntity()
    {
        var entities = GameObject.FindGameObjectsWithTag("Entity");

        return entities.FirstOrDefault(entity => Vector3.Distance(transform.position, entity.transform.position) <= Range);
    }
}
