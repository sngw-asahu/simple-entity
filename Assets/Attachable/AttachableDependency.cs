﻿using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Attachable
{
    public class AttachableDependency : LifetimeScope
    {
        [SerializeField] public EntityView view;
        
        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterComponentInNewPrefab(view, Lifetime.Transient);
            builder.RegisterFactory<Vector3, EntityView>(container =>
            {
                return v =>
                {
                    var entityView = container.Resolve<EntityView>();
                    entityView.transform.position = v;
                    return entityView;
                };
            }, Lifetime.Scoped);
            builder.RegisterEntryPoint<EntityPresenter>();
        }
    }
}