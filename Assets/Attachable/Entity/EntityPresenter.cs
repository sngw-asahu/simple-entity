﻿using System;
using System.Collections.Generic;
using Game.Controller;
using UnityEngine;
using VContainer;
using VContainer.Unity;

// ReSharper disable once CheckNamespace
public class EntityPresenter : ITickable
{
    private readonly EntityController _controller;
    private readonly Func<Vector3, EntityView> _factory;
    private float _timer;

    [Inject]
    public EntityPresenter(EntityController controller, Func<Vector3, EntityView> factory)
    {
        _controller = controller;
        _factory = factory;
    }
    
    public void Tick()
    {
        _timer += Time.deltaTime;
        
        if (_timer < 2f) return;
        ShowEntityView();
        _timer = 0f;
    }
    
    private void ShowEntityView()
    {
        var entity = _controller.GetEntity();
        var view = _factory.Invoke(entity.CurrentTile()!.GetVector3(0.5f));
        view.SetModel(entity);
    }
}
