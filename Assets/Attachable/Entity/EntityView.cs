using System.Collections;
using Game.Domain.Entity;
using Game.Domain.Tile;
using Unity.VisualScripting;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class EntityView : MonoBehaviour
{
    private const float MoveSpeed = 2f;
    private bool _isMoving;
    public bool needsDestroy;
    private int _destroyCnt;
    private EntityModel _model;

    public void SetModel(EntityModel model) => _model = model;

    private void SetDestroyFlag()
    {
        needsDestroy = true;
        _destroyCnt = 60;
    }

    private void Start()
    {
        needsDestroy = false;
    }

    private void Update()
    {
        if (_model == null) return;
        if (_isMoving) return;
        if (needsDestroy)
        {
            --_destroyCnt;
            
            transform.localScale -= new Vector3(0.005f, 0.005f, 0.005f);

            if (_destroyCnt <= 0)
            {
                Destroy(this.GameObject());
            }

            return;
        }

        var next = _model.NextTile();

        if (next == null)
        {
            SetDestroyFlag();
            return;
        }
        Move(next);
    }
    
    private void Move(TileModel targetTile)
    {
        StartCoroutine(RunMovement(targetTile.GetVector3(0.5f)));
    }
    
    private IEnumerator RunMovement(Vector3 targetPos)
    {
        _isMoving = true;
        
        while ((targetPos - transform.position).sqrMagnitude > Mathf.Epsilon)
        {
            var currentPos = transform.position;
            
            transform.position = Vector3.MoveTowards(
                current: currentPos,
                target: targetPos,
                maxDistanceDelta: MoveSpeed * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);
        _isMoving = false;
    }
    
    public void OnCollisionEnter(Collision other)
    {
        var hitObj = other.gameObject;

        if (!hitObj.CompareTag("Bullet")) return;
        SetDestroyFlag();
        Destroy(hitObj);
    }
}
