﻿using System.Collections.Generic;
using System.Linq;
using Game.Domain.Tile;
using UnityEngine;

namespace Game.Domain.Entity.Route
{
    public record TileRouteModel(List<TileModel> Tiles)
    {
        public TileRouteModel(TileRouteModel origin)
        {
            var newTiles = origin.Tiles.Select(tile => 
                tile with { Vec = new Vector2(tile.Vec.x, tile.Vec.y) }).ToList();

            Tiles = newTiles;
        }
    }
}