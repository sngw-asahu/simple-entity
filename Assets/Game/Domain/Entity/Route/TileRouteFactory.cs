﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Game.Domain.Tile;
using VContainer;
using TileModel = Game.Domain.Tile.TileModel;


namespace Game.Domain.Entity.Route
{
    public class TileRouteFactory
    {
        private readonly TileService _tileService;

        [Inject]
        public TileRouteFactory(TileService tileService) => _tileService = tileService;

        public IEnumerable<TileRouteModel> SearchRoutes(int limit)
        {
            var startTile = _tileService.GetTileByType(TileTypeEnum.Start) ??
                            throw new NoNullAllowedException("探索の開始地点が見つかりません");
            var goalTile = _tileService.GetTileByType(TileTypeEnum.Goal) ??
                           throw new NoNullAllowedException("探索の終了地点が見つかりません");

            var routes = new List<TileRouteModel>
            {
                new(new List<TileModel> { startTile })
            };

            for (var i = 0; limit > i; ++i)
            {
                var nextRoutes = new List<TileRouteModel>();

                foreach (var route in routes)
                {
                    var currentTile = route.Tiles.Last();
                    
                    foreach (var nextTile in _tileService.GetAdjacentTiles(currentTile))
                    {
                        if (route.Tiles.Contains(nextTile)) continue; // 今まで来た道だったらスキップ
                        var newRoute = new TileRouteModel(route);  // 新規に更新されたルートを作成
                        newRoute.Tiles.Add(nextTile);

                        if (nextTile == goalTile) yield return newRoute;
                        nextRoutes.Add(newRoute);
                    }
                }

                if (0 >= nextRoutes.Count) break;
                routes = nextRoutes;
            }
        }
    }
}