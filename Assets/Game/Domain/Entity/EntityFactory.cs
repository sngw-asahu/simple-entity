﻿using System.Collections.Generic;
using System.Linq;
using Game.Domain.Entity.Route;
using JetBrains.Annotations;
using VContainer;
using Random = System.Random;

namespace Game.Domain.Entity
{
    public class EntityFactory
    {
        private readonly List<TileRouteModel> _routeList;

        [Inject]
        public EntityFactory(TileRouteFactory factory)
        {
            _routeList = factory.SearchRoutes(300).ToList();
        }
        
        [CanBeNull]
        public EntityModel MakeEntity()
        {
            var route = _routeList[(new Random()).Next(0, _routeList.Count() - 1)];
            return new EntityModel(route);
        }
    }
}