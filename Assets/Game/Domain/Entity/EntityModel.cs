﻿using System.Linq;
using Game.Domain.Entity.Route;
using Game.Domain.Tile;
using JetBrains.Annotations;

namespace Game.Domain.Entity
{
    public record EntityModel
    {
        private int CurrentPositionIndex {  get; set; }
        private TileRouteModel TileRoute {  get; }

        public EntityModel(TileRouteModel tileRoute, int currentPositionIndex = 0)
        {
            CurrentPositionIndex = currentPositionIndex;
            TileRoute = tileRoute;
        }

        [CanBeNull]
        public TileModel CurrentTile()
        {
            return TileRoute.Tiles.FirstOrDefault();
        }

        [CanBeNull]
        public TileModel NextTile()
        {
            ++CurrentPositionIndex;
            return TileRoute.Tiles.Count > CurrentPositionIndex ? TileRoute.Tiles[CurrentPositionIndex] : null;
        }

        public bool IsGoaled()
        {
            return CurrentPositionIndex == TileRoute.Tiles.Count;
        }
    };
}