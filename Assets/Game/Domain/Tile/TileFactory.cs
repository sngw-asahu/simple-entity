﻿using System;
using System.Linq;
using UnityEngine;

namespace Game.Domain.Tile
{
    public class TileFactory
    {
        private TileModel[] _tiles = Array.Empty<TileModel>();

        public TileFactory()
        {
            UpdateTiles();
        }
        
        private void UpdateTiles()  // Tileが動的に変更するユースケースがあったらpublicへ
        {
            var tileObjs = GameObject.FindGameObjectsWithTag("Tile");
            _tiles = new TileModel[tileObjs.Length];

            for (var i = 0; i < tileObjs.Length; i++)
            {
                var tileObj = tileObjs[i];
                var pos = tileObj.transform.position;

                var type = tileObj.name switch
                {
                    "Start" => TileTypeEnum.Start,
                    "Goal" => TileTypeEnum.Goal,
                    _ => TileTypeEnum.Road
                };

                _tiles[i] = new TileModel(new Vector2(pos.x, pos.z), type);
            }
        }

        public TileModel GetTileByVector2(Vector2 vec)
        {
            return _tiles.FirstOrDefault(tile => tile.Vec.Equals(vec));
        }

        public TileModel GetTileByType(TileTypeEnum type)
        {
            return _tiles.FirstOrDefault(tile => tile.Type.Equals(type));
        }
    }
}