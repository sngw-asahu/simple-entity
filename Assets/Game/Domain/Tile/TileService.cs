﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using VContainer;

namespace Game.Domain.Tile
{
    public class TileService
    {
        private readonly TileFactory _factory;
        
        [Inject]
        public TileService(TileFactory factory) => _factory = factory;

        [CanBeNull]
        public TileModel GetTileByType(TileTypeEnum type)
        {
            return _factory.GetTileByType(type);
        }
        
        public IEnumerable<TileModel> GetAdjacentTiles(TileModel tile)
        {
            var vec = tile.Vec;
            var searchVecs = new Vector2[]
            {
                new (vec.x - 1, vec.y),
                new (vec.x, vec.y - 1),
                new (vec.x + 1, vec.y),
                new (vec.x, vec.y + 1)
            };

            foreach (var searchVec in searchVecs)
            {
                var searchTile = _factory.GetTileByVector2(searchVec);

                if (searchTile == null) continue;
                yield return searchTile;
            }
        }
    }
}