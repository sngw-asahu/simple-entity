﻿using UnityEngine;

namespace Game.Domain.Tile
{
    public record TileModel(Vector2 Vec, TileTypeEnum Type)
    {
        public Vector3 GetVector3(float y = 0f)
        {
            return new Vector3(Vec.x, y, Vec.y);
        }
    }
}