﻿namespace Game.Domain.Tile
{
    public enum TileTypeEnum
    {
        Road,
        Start,
        Goal
    }
}