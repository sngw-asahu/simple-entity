﻿using Game.Controller;
using Game.Domain.Entity;
using Game.Domain.Entity.Route;
using Game.Domain.Tile;
using VContainer;
using VContainer.Unity;

namespace Game
{
    public class TileDependency : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<TileService>(Lifetime.Scoped);
            
            builder.Register<TileRouteFactory>(Lifetime.Singleton);
            builder.Register<TileFactory>(Lifetime.Singleton);
            builder.Register<EntityFactory>(Lifetime.Singleton);

            builder.Register<EntityController>(Lifetime.Singleton);
        }
    }
}