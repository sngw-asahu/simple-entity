﻿using Game.Domain.Entity;
using VContainer;

namespace Game.Controller
{
    public class EntityController  // システム上そこまで債務がないので現状はなくてもよい
    {
        private readonly EntityFactory _entityFactory;

        [Inject]
        public EntityController(EntityFactory entityFactory) => _entityFactory = entityFactory;

        public EntityModel GetEntity()
        {
            return _entityFactory.MakeEntity();
        }
    }
}